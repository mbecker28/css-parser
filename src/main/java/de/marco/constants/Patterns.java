package de.marco.constants;

import java.util.regex.Pattern;

public class Patterns {

	public static Pattern BLOCK_PATTERN = Pattern.compile("([a-zA-Z]+|(\\.|#){1}[a-zA-Z0-9\\-])+[a-zA-Z\\-\\.#,0-9:\\[\\]\"=\\(\\)\t ]*\\{[\t!#a-zA-Z:0-9,;\\-\\(\\)'\\. ]*\\}");

	public static Pattern PROPERTY_PATTERN = Pattern.compile("[a-zA-Z\\- ]*[:]+[ a-zA-Z0-9#\\(\\)\\-'\\.,]*(!)?(;)?");

	public static Pattern SUB_SELECTOR_CHILD_PATTERN = Pattern.compile("([a-zA-Z]+|(\\.|#){1}[a-zA-Z0-9\\-]*)");

	public static Pattern SELECTOR_CHILD_PATTERN = Pattern
	    .compile("([a-zA-Z]+|(\\.|#){1}[a-zA-Z0-9\\-]*)+((\\.|#){1}[a-zA-Z-]*)?([:]+[a-zA-Z\\-]+(\\([a-zA-Z0-9' \t]+\\))|\\[[a-zA-Z=\" ]+\\])?");

	public static Pattern SELECTOR_SUB_INFO_PATTERN = Pattern.compile("([:]+[a-zA-Z\\-]+(\\([a-zA-Z0-9' \t]+\\))|\\[[a-zA-Z=\" ]+\\])+");

}
