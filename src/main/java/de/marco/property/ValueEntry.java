package de.marco.property;

public class ValueEntry {

	private String value;

	protected ValueEntry(final String value) {
		this.value = value;
	}

	public ValueEntry() {

	}

	public void setValue(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.toLowerCase().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ValueEntry other = (ValueEntry) obj;
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equalsIgnoreCase(other.value)) {
			return false;
		}
		return true;
	}

}
