package de.marco.property;

import java.util.LinkedList;
import java.util.List;

public class Property {

	private String key;

	private List<PropertyValue> valueList = new LinkedList<PropertyValue>();

	protected Property(final String property) {
		parse(removeInvalidChars(property));
	}

	public Property() {

	}

	public void setKey(final String name) {
		key = name;
	}

	public String getKey() {
		return key;
	}

	private void parse(final String property) {
		final String[] prop = property.split(":");
		key = prop[0].trim();
		final String value = prop[1];
		if (value.contains(",")) {
			final String[] values = value.split(",");
			for (final String valueE : values) {
				valueList.add(new PropertyValue(valueE.trim()));
			}
		} else {
			valueList.add(new PropertyValue(value.trim()));
		}
	}

	public List<PropertyValue> getPropertyValues() {
		return valueList;
	}

	public PropertyValue getSinglePropertyValue() {
		if (valueList.isEmpty()) {
			valueList.add(new PropertyValue());
		}
		return valueList.get(0);
	}

	public void addSinglePropertyValue(final PropertyValue propertyValue) {
		valueList.clear();
		valueList.add(propertyValue);
	}

	public void addPropertyValues(final List<PropertyValue> values) {
		valueList = values;
	}

	public ValueEntry getFirstEntry() {
		return valueList.get(0).getEntryList().get(0);
	}

	private String removeInvalidChars(final String value) {
		return value.replaceAll(";", "").trim();
	}

	public String getPlain(final boolean formatted) {
		final StringBuilder builder = new StringBuilder();
		builder.append(key);
		builder.append(":");
		boolean first = true;
		for (final PropertyValue value : valueList) {
			if (formatted) {
				builder.append(" ");
			}
			if (!first) {
				builder.append(",");
			}
			builder.append(value.getPlain(formatted));
			first = false;
		}
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((valueList == null) ? 0 : valueList.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Property other = (Property) obj;
		if (key == null) {
			if (other.key != null) {
				return false;
			}
		} else if (!key.equalsIgnoreCase(other.key)) {
			return false;
		}
		if (valueList == null) {
			if (other.valueList != null) {
				return false;
			}
		} else if (!valueList.equals(other.valueList)) {
			return false;
		}
		return true;
	}

}
