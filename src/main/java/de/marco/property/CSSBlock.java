package de.marco.property;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;

import de.marco.constants.Patterns;
import de.marco.selector.SelectorContainer;

public class CSSBlock {

	private final List<SelectorContainer> selectors = new LinkedList<SelectorContainer>();

	private final List<Property> proteries = new LinkedList<Property>();

	public void parse(final String block) {
		final String selectorString = block.substring(0, block.indexOf("{") - 1);
		final String[] selectorArray = selectorString.split(",");
		for (final String selector : selectorArray) {
			selectors.add(new SelectorContainer(selector));
		}
		final String blockBody = block.substring(block.indexOf("{"));
		final Matcher propertyMatcher = Patterns.PROPERTY_PATTERN.matcher(blockBody);
		while (propertyMatcher.find()) {
			proteries.add(new Property(propertyMatcher.group()));
		}
	}

	public String getPlain(final boolean formatted) {
		final StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (final SelectorContainer selector : selectors) {
			if (!first) {
				builder.append(",");
			}
			builder.append(selector.getPlain(formatted));
			first = false;
		}
		if (formatted) {
			builder.append(" ");
		}
		builder.append("{");
		boolean firstProperty = true;
		for (final Property property : proteries) {
			if (!firstProperty) {
				builder.append(";");
			}
			if (formatted) {
				builder.append("\n\t");
			}
			builder.append(property.getPlain(formatted));
			firstProperty = false;
		}
		if (formatted) {
			builder.append("\n");
		}
		builder.append("}");
		if (formatted) {
			builder.append("\n");
		}
		return builder.toString();
	}

	public List<Property> getProperties(final String[] properties) {
		final List<Property> list = new LinkedList<Property>();
		for (final Property property : proteries) {
			for (final String propName : properties) {
				if (propName.equalsIgnoreCase(property.getKey())) {
					list.add(property);
				}
			}
		}
		return list;
	}

	public void addProperty(final Property property) {
		proteries.add(property);
	}

	public void removeProperty(final Property property) {
		final List<Property> list = new LinkedList<Property>();
		for (final Property property2 : proteries) {
			if (property2.equals(property)) {
				list.add(property2);
			}
		}
		proteries.removeAll(list);
	}

}
