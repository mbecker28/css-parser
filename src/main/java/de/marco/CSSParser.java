package de.marco;

import java.io.BufferedReader;
import java.io.Reader;
import java.util.regex.Matcher;

import de.marco.constants.Patterns;
import de.marco.property.CSSBlock;

public class CSSParser {

	public CSSDocument parse(final Reader reader) {
		final CSSDocument document = new CSSDocument();
		try {
			String cssFile = "";
			final BufferedReader bufferedReader = new BufferedReader(reader);
			String line = null;
			do {
				line = bufferedReader.readLine();
				if (line != null) {
					cssFile += line;
				}
			} while (line != null);
			final Matcher matcher = Patterns.BLOCK_PATTERN.matcher(cssFile);
			while (matcher.find()) {
				final CSSBlock block = new CSSBlock();
				block.parse(matcher.group());
				block.toString();
				document.addBlock(block);
			}
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
		return document;
	}

}
