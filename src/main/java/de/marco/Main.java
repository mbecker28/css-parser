package de.marco;

import java.io.File;
import java.io.FileReader;

public class Main {

	public static void main(final String[] args) {
		try {
			final CSSParser parser = new CSSParser();
			final File file = new File("/home/marco/alternate-min.css");
			final FileReader reader = new FileReader(file);
			final CSSDocument document = parser.parse(reader);
			System.out.print(document.toString());
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
	}
}
