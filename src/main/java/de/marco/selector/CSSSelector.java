package de.marco.selector;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;

import de.marco.constants.Patterns;
import de.marco.selector.types.SelectorType;

public class CSSSelector {

	private CSSSelector child;

	private final List<CSSSelector> subSelectorList = new LinkedList<CSSSelector>();

	private String selector;

	private String subInfo = "";

	public CSSSelector(final String childSelector) {
		parse(childSelector);
	}

	public SelectorType getType() {
		if (selector.startsWith(".")) {
			return SelectorType.CLASS;
		}
		if (selector.startsWith("#")) {
			return SelectorType.ID;
		}
		return SelectorType.TAG;
	}

	private void parse(final String selector) {
		final Matcher subInfoMatcher = Patterns.SELECTOR_SUB_INFO_PATTERN.matcher(selector);
		if (subInfoMatcher.find()) {
			subInfo = subInfoMatcher.group();
		}
		final Matcher matcher = Patterns.SUB_SELECTOR_CHILD_PATTERN.matcher(selector.replace(subInfo, ""));
		boolean first = true;
		while (matcher.find()) {
			if (first) {
				this.selector = matcher.group();
			} else {
				subSelectorList.add(new CSSSelector(matcher.group()));
			}
			first = false;
		}
	}

	public void addChild(final CSSSelector child) {
		this.child = child;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(selector);
		for (final CSSSelector subSelector : subSelectorList) {
			builder.append(subSelector.toString());
		}
		if (child != null) {
			builder.append(" ");
			builder.append(child);
		}
		builder.append(subInfo);
		return builder.toString();
	}

}
