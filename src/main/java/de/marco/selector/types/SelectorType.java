package de.marco.selector.types;

public enum SelectorType {

	ID,
	TAG,
	CLASS;
}
