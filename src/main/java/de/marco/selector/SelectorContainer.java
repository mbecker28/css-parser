package de.marco.selector;

import java.util.regex.Matcher;

import de.marco.constants.Patterns;

public class SelectorContainer {

	private CSSSelector startSelector;

	public SelectorContainer(final String selector) {
		parse(removeInvalidChars(selector));
	}

	private void parse(final String selector) {
		final Matcher matcher = Patterns.SELECTOR_CHILD_PATTERN.matcher(selector);
		boolean first = true;
		CSSSelector last = null;
		while (matcher.find()) {
			final CSSSelector sel = new CSSSelector(matcher.group());
			if (first) {
				startSelector = sel;
			} else {
				last.addChild(sel);
			}
			last = sel;
			first = false;
		}
	}

	private String removeInvalidChars(final String value) {
		return value.replaceAll(",", "").trim();
	}

	public String getPlain(final boolean formatted) {
		return startSelector.toString();
	}

}
