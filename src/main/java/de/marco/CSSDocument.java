package de.marco;

import java.util.LinkedList;
import java.util.List;

import de.marco.property.CSSBlock;

public class CSSDocument {

	private final List<CSSBlock> cssBlockList = new LinkedList<CSSBlock>();

	public void addBlock(final CSSBlock block) {
		cssBlockList.add(block);
	}

	public List<CSSBlock> getCssBlockList() {
		return cssBlockList;
	}

	public String getPlain(final boolean formatted) {
		final StringBuilder builder = new StringBuilder();
		for (final CSSBlock block : cssBlockList) {
			builder.append(block.getPlain(formatted));
		}
		return builder.toString();
	}

}
