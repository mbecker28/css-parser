import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.Test;

import de.marco.CSSDocument;
import de.marco.CSSParser;

public class ParserTest {

	@Test
	public void test() throws IOException {
		final CSSParser cssParser = new CSSParser();
		final InputStream stream = ClassLoader.getSystemResourceAsStream("start.css");
		final CSSDocument document = cssParser.parse(new InputStreamReader(stream));
		final String actual = document.getPlain(false);
		final String expected = getExcpectetd();
		System.out.println("E:" + expected);
		System.out.println("A:" + actual);
		assertEquals(expected, actual);
	}

	private String getExcpectetd() throws IOException {
		final InputStream stream = ClassLoader.getSystemResourceAsStream("result.css");
		final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		String res = "";
		String line = null;
		do {
			line = reader.readLine();
			if (line != null) {
				res += line;
			}
		} while (line != null);
		return res;
	}

}
